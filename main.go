package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/jessevdk/go-flags"
	"gitlab.com/zrice/common-ng/analyzer"
	"gitlab.com/zrice/common-ng/options"
)

const (
	pathToScanner        = "gitleaks"
	pathToGitleaksReport = "gitleaks-report.json"
)

// Secret represents a gitleaks leak
type Secret struct {
	Line     string `json:"line"`
	Offender string `json:"offender"`
	Rule     string `json:"rule"`
	Commit   string `json:"commit"`
	File     string `json:"file"`
	Message  string `json:"commitMessage"`
	Author   string `json:"author"`
	Date     string `json:"date"`
}

// Options represents flags passed in via cli options
type Options struct {
	options.BaseOptions
	Run struct {
		Historic   bool    `env:"HISTORIC_SCAN" long:"historic"`
		CommitTo   string  `env:"SECRET_DETECTION_COMMIT_TO" long:"commit-to"`
		CommitFrom string  `env:"SECRET_DETECTION_COMMIT_FROM" long:"commit-from"`
		Entropy    float64 `env:"SECRET_DETECTION_ENTROPY_LEVEL" long:"entropy"`
	} `command:"run" help:"run command"`
	Search struct {
	} `command:"search" help:"search command"`
	Convert struct {
		ConvertTarget string `env:"CONVERT_TARGET" default:"" long:"target"`
	} `command:"convert" help:"convert command"`
}

// SecretDetection is an secret detection analyzer
type SecretDetection struct {
	Name string
	opts Options
}

func main() {
	// Acquire options from the CLI
	var opts Options
	_, err := flags.Parse(&opts)
	if err != nil {
		os.Exit(-1)
	}

	// Define a new analyzer
	var sda analyzer.Analyzer = SecretDetection{
		Name: "secret-detection",
		opts: opts,
	}

	// Determine which command to run
	switch strings.ToLower(os.Args[1]) {
	case options.Run:
		runGitleaks(opts)
		f, err := os.Open(pathToGitleaksReport)
		defer os.Remove(pathToGitleaksReport)
		report, err := sda.Analyze(f)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(report)
	case options.Convert:
		fmt.Println("convert")
		f, err := os.Open(opts.TargetDir)
		if err != nil {
			log.Fatal(err)
		}
		report, err := sda.Analyze(f)
		fmt.Println(report)
	case options.Search:
		//  TODO
		fmt.Println("search")
	}
}

// runGitleaks will run gitleaks. This function is only reached when the ./analyzer run command is set
func runGitleaks(opts Options) error {
	args := []string{
		fmt.Sprintf("--repo-path=%s", opts.TargetDir),
		fmt.Sprintf("--report=%s", pathToGitleaksReport),
	}
	return exec.Command(pathToScanner, args...).Run()
}
