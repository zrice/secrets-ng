module gitlab.com/secrets-ng

go 1.14

require (
	github.com/jessevdk/go-flags v1.4.0
	gitlab.com/zrice/common-ng v0.0.0-20200709231508-f5036f862b2f
)
