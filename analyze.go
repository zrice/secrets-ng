package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/zrice/common-ng/vulnerability"
	"io"
)

// Analyze reads a reader then returns a report and error
func (sda SecretDetection) Analyze(r io.Reader) (*vulnerability.Report, error) {
	var (
		report  vulnerability.Report
		secrets []Secret
	)
	err := json.NewDecoder(r).Decode(&secrets)
	if err != nil {
		return nil, err
	}

	for _, secret := range secrets {
		fmt.Println(secret.Line)
		// TODO implement "toIssues" logic
		//sourceCode := secret.Line
		//filePath := secret.File
		//line := 1
		//if !sda.opts.Run.Historic {
		//	line, err = extractLineNumber(secret)
		//}


	}
	return &report, nil
}

//func extractLineNumber(secret Secret) (int, error) {
//	f, err := os.Open(secret.File)
//	if err != nil {
//		return -1, err
//	}
//	reader := bufio.NewReader(f)
//
//	return 0, nil
//}
